
import re

# match definition 
defline_re = re.compile("#define\s+(?P<defname>[^\s]+)\s+(?P<value>[^\s]+)", re.DOTALL)

# match definition and comment (optional)
defline2_re = re.compile("#define\s+(?P<defname>[^\s]+)\s+(?P<value>[^\s]+)(?:\s+\/\*(?P<comment>.*?)\*\/)?$", re.DOTALL)

def import_hdef(filename, update_module=None, error_mode=False):
    # import definitions from handel include files
    # in non error_mode:
    #      import symbols, defined with #define, from include filename into importing module
    # in error_mode: 
    #    a dictionary indexed by the error number and including comment in #define line is returned
    
    buflines = open(filename).readlines()

    d = {}
    error_d = {}

    for line in buflines:
        line = line.strip()
        if not line.startswith("#define"):
            continue

        if error_mode:
            mat = defline2_re.match(line)
        else:
            mat = defline_re.match(line)

        if mat:
            defname = mat.group("defname")
            # values are always numeric (int, hexa or float)
            value = mat.group("value")
            try:
                numvalue = int(value,0)
            except ValueError:
                if value.startswith("("):
                    # values (3000) and (4000) in handel_errors.h ignored
                    continue
                numvalue = float(value)
            except BaseException as e:
                raise Exception("Error importing define constants from handel includes: " + str(e))
             
            if error_mode:
                comment = mat.group("comment")
                d[defname] = numvalue
                error_d[numvalue] = (defname, comment)
            else:
                d[defname] = numvalue

    if update_module:
        import sys
        mod = sys.modules[update_module]
        mod.__dict__.update(d)

    if error_mode:
        return error_d
   

