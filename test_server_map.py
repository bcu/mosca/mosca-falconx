
from tango import DeviceProxy

import time
import sys

config_file = "cyril6.ini"
config_file = "double_a_c.ini"
config_file = "XiaDxpSmall_HR_17keV_082022.ini"
config_file = "XiaDxpBig_HR_17keV_082022.ini"
# two detectors
config_file = "Ardesia16_HR_17keV_082022.ini"

points = int(sys.argv[1])
buffer_size = int(sys.argv[2])

dev = DeviceProxy("id16a/falconx/fx1")
dev.DevXiaSetConfig(config_file)
dev.DevXiaSetAcqPar([4,0,points,buffer_size])

dev.DevXiaSetFilePar(['vic3','test2','1','3','5','1'])
dev.saving_mode = "AUTO"
#dev.saving_folder = "vicdata"
#dev.saving_prefix = "test"

dev.DevXiaStart()

# dev.detector_mode = "MAP"
# dev.number_points = points
# dev.buffer_size = points
# hi.set_map_pars(0,points,buffer_size)
# dev.startAcq()

# ctrl
print(f"Starting xia in map mode for {points} points - buffer_size: {buffer_size} ")
time.sleep(1)

t0 = None
try:
    while str(dev.State()) != 'RUNNING':
        print("waiting.. state:%s" % str(dev.State))
        time.sleep(0.5)
    print("xia started")
    read_pixel = read_pixels_old = 0
    expected_pixels = dev.map_number_pixels
    print(f"Pixels read: {read_pixel: 4d}/{expected_pixels:4d}\r", end="")
    t0 = None
    while str(dev.State()) == 'RUNNING':
        read_pixel = dev.map_read_pixel
        current_pixel = dev.map_current_pixel
        time.sleep(0.1)
        if current_pixel != read_pixels_old:
             if current_pixel != 0 and t0 is None:
                 t0 = time.time()
                 fpx = current_pixel
             print(f"Pixels read: {current_pixel: 4d}/{expected_pixels:4d}\r", end="")
             read_pixels_old = current_pixel
    time.sleep(1)
    read_pixel = dev.map_read_pixel
    print(f"Pixels read: {read_pixel: 4d}/{expected_pixels:4d} - ACQ DONE", end="\n")
    elapsed = time.time() - t0
    elap_px = 1000 * elapsed / (read_pixel - fpx) 
    print(f" Total time: {elapsed:03.2f} secs, per pixel {elap_px:02.2f} msecs per pixel")
except KeyboardInterrupt:
    dev.DevXiaStop()
